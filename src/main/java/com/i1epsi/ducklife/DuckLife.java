/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.i1epsi.ducklife;


public class DuckLife {

    public static void main(String[] args) {
        //System.out.println("Hello World!");
        Duck mallard = new MallardDuck();
        Duck redhead = new RedHeadDuck();
        
        //mallard.fly();
        //mallard.display();
        //mallard.swim();
        redhead.quack();
        
        //redhead.fly();
        //redhead.display();
        //redhead.swim();
    }
    
}