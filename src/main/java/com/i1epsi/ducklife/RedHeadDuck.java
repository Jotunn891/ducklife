/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.ducklife;

/**
 *
 * @author TVall
 */
public class RedHeadDuck extends Duck{
    public RedHeadDuck(){
        quackBehavior = new Whistle();
    }
    
    public void display(){
        System.out.println("I'm a red head duck!");
    }
}
