/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.ducklife;

/**
 *
 * @author TVall
 */
public class MallardDuck extends Duck{
    public void display(){
        System.out.println("I'm a mallard duck!");
    }
}
