/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.ducklife;


public abstract class Duck {
    QuackBehavior quackBehavior;
    
    public void fly(){
        //TODO
    }
    
    public void quack(){
        //TODO
        quackBehavior.quack();
    }
    
    public abstract void display();
    
    public void swim(){
        //TODO
    }
}
